<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Charger des styles CSS dans l’espace public
 *
 * @pipeline insert_head_css
 * @param string $flux Code html des styles CSS à charger
 * @return string Code html complété
 */
function badge_don_insert_head_css($flux) {
	$flux .= '<link rel="stylesheet" href="' . find_in_path('badge_don.css') . '" />';
	return $flux;
}

/**
 * Insérer le code du badge à la fin des pages HTML
 * @param string $page
 * @return string
 */
function badge_don_affichage_final($page) {
	// ne rien faire si la page en cours n'est pas du html
	if (!$GLOBALS['html']) {
		return $page;
	}

	$badge = recuperer_fond('modeles/badge_don', []);
	// Insertion du badge avant la fermeture du body
	if (!strpos($page, 'id="badge_don"')) {
		$page = preg_replace(',</body>,i', "$badge\n" . '\0', $page, 1);
	}

	return $page;
}
